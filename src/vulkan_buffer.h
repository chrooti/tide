#pragma once

#include "vulkan_context.h"

namespace tide::vulkan {

struct Buffer {
    VkBuffer handle;
    VkDeviceMemory memory;
};

VkResult create_buffer(VkBuffer* buffer, Context* ctx, size_t size, VkBufferUsageFlags usage_flags) {
    VkBufferCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = usage_flags,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    return vkCreateBuffer(ctx->device, &create_info, nullptr, buffer);
}

VkResult allocate_memory(VkDeviceMemory* memory, Context* ctx, size_t size, uint32_t memory_type_idx) {
    VkMemoryAllocateInfo allocate_info = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = size,
        .memoryTypeIndex = memory_type_idx,
    };

    return vkAllocateMemory(ctx->device, &allocate_info, nullptr, memory);
}

} // namespace tide::vulkan

namespace tide::vulkan::buffer {

enum struct Stage {
    CREATION,
    ALLOCATION,
    BINDING,
    LAST,
};

void destroy(Buffer* buf, Context* ctx, Stage stage = Stage::LAST) {
    switch (stage) {
    case Stage::LAST:
    case Stage::BINDING:
    case Stage::ALLOCATION:
        vkFreeMemory(ctx->device, buf->memory, nullptr);
    case Stage::CREATION:
        vkDestroyBuffer(ctx->device, buf->handle, nullptr);
    }
}

template<typename Logger>
DECLARE_STEPPED(void, create, Buffer* buf, Logger* logger, Context* ctx, VkDeviceSize size, VkBufferUsageFlags usage_flags, VkMemoryPropertyFlags mem_property_flags) {
    VkResult res;

    res = create_buffer(&buf->handle, ctx, size, usage_flags);
    if (res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkCreateBuffer", res);
        THROW();
    }

    VkMemoryRequirements mem_requirements;
    vkGetBufferMemoryRequirements(ctx->device, buf->handle, &mem_requirements);

    VkPhysicalDeviceMemoryProperties mem_properties;
    vkGetPhysicalDeviceMemoryProperties(ctx->physical_device, &mem_properties);

    uint32_t wanted_type = mem_requirements.memoryTypeBits;

    uint32_t mem_type_idx;
    for (uint32_t i = 0; i < mem_properties.memoryTypeCount; i++) {
        if ((wanted_type & (1 << i)) != 0 && (mem_properties.memoryTypes[i].propertyFlags & mem_property_flags) != 0) {
            mem_type_idx = i;
            goto memory_type_found;
        }
    }

    Logger::log(logger, "Couldn't find a suitable memory type");
    destroy(buf, ctx, Stage::CREATION);
    THROW();

memory_type_found:;

    res = allocate_memory(&buf->memory, ctx, mem_requirements.size, mem_type_idx);
    if (res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkAllocateMemory", res);
        destroy(buf, ctx, Stage::CREATION);
        THROW();
    }

    res = vkBindBufferMemory(ctx->device, buf->handle, buf->memory, 0);
    if (res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkBindBufferMemory", res);
        destroy(buf, ctx, Stage::ALLOCATION);
        THROW();
    }
}

} // namespace tide::vulkan::buffer