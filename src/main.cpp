#include <signal.h>

#include "logger.h"
#include "platform_wayland.h"
#include "platform_xcb.h"
#include "settings.h"
#include "vulkan_window.h"

namespace tide {

/* Data types */

template<typename P>
struct Application {
    using Platform = P;

    Logger logger;

    Settings settings;

    typename Platform::WindowSystem ws;
    typename Platform::MainWindow plat_main_window;

    vulkan::Context vk_ctx;
    vulkan::Window vk_main_window;

    bool should_close;
};

/* App init/destroy */

template<typename Platform>
void destroy(Application<Platform>* app) {
    vulkan::window::destroy_rest(&app->vk_main_window, &app->vk_ctx);
    vulkan::context::destroy_rest(&app->vk_ctx);
    vulkan::window::destroy_surface(&app->vk_main_window, &app->vk_ctx);
    vulkan::context::destroy_instance(&app->vk_ctx);
    Platform::destroy_main_window(&app->ws, &app->plat_main_window);
    Platform::destroy_window_system(&app->ws);

    exit(0);
}

// used only by the signal handler, DON'T use it for anything else
__attribute__((__used__)) static void* global_app;

template<typename Platform>
void sigint_handler(int) {
    auto app = reinterpret_cast<Application<Platform>*>(global_app);

    app->should_close = true;
}

template<typename Platform>
void init(Application<Platform>* app) {
    app->should_close = false;

    // handle sigint
    struct sigaction sigint_action = {
        .sa_handler = sigint_handler<Platform>
    };
    sigaction(SIGINT, &sigint_action, nullptr);

    Logger::init(&app->logger);

    settings::read_settings(&app->settings);

    // TODO: register event handlers from UI
    // EventHandler* event_handlers;
    // ui::get_event_handlers(&event_handlers);
    // graphics::register_events(&app->graphics, &event_handlers);

    CALL_STEPPED(Platform::init_window_system, cleanup_final, &app->ws);
    CALL_STEPPED(Platform::init_main_window, cleanup_ws, &app->ws, &app->plat_main_window);
    CALL_STEPPED(vulkan::context::init_instance<Platform::surface_extension>, cleanup_platform_window, &app->vk_ctx, &app->logger, &app->settings);
    CALL_STEPPED(vulkan::window::init_surface<Platform>, cleanup_vk_ctx_instance, &app->vk_main_window, &app->logger, &app->vk_ctx, &app->ws, &app->plat_main_window);
    CALL_STEPPED(vulkan::context::init_rest, cleanup_vk_window_surface, &app->vk_ctx, &app->logger, &app->settings, app->vk_main_window.surface);
    CALL_STEPPED(vulkan::window::init_rest<Platform>, cleanup_vk_ctx_rest, &app->vk_main_window, &app->logger, &app->vk_ctx, &app->ws, &app->plat_main_window);


    asm goto("" :::: cleanup_vk_window_rest);

    return;

cleanup_vk_window_rest:
    vulkan::window::destroy_rest(&app->vk_main_window, &app->vk_ctx);

cleanup_vk_ctx_rest:
    vulkan::context::destroy_rest(&app->vk_ctx);

cleanup_vk_window_surface:
    vulkan::window::destroy_surface(&app->vk_main_window, &app->vk_ctx);

cleanup_vk_ctx_instance:
    vulkan::context::destroy_instance(&app->vk_ctx);

cleanup_platform_window:
    Platform::destroy_main_window(&app->ws, &app->plat_main_window);

cleanup_ws:
    Platform::destroy_window_system(&app->ws);

cleanup_final:
    exit(0);
}

/* Loop */

template<typename Platform>
void run(Application<Platform>* app) {
    while(!app->should_close) {

        CALL_STEPPED(vulkan::window::draw_frame<Platform>, end, &app->vk_main_window, &app->logger, &app->vk_ctx, &app->ws, &app->plat_main_window);
        app->should_close = Platform::handle_events(&app->ws);
//        sleep(1);

    }

end:;
    // TODO: remake this helper in vulkan_context
    vkDeviceWaitIdle(app->vk_ctx.device);
}

} // namespace tide


int main() {
    tide::Application<tide::platform::Xcb> app = {};

    tide::init(&app);
    tide::run(&app);
    tide::destroy(&app);
    return 0;
}
