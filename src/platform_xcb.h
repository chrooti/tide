#pragma once

/* See the comment in the platform_wayland.h header,
 * since surprisingly wayland is made by the same
 * brain-damaged people that made X
 */

#include <stdlib.h>
#include <xcb/xcb_icccm.h>

#include "macros.h"
#include "vulkan.h"

namespace tide::platform {

struct Xcb {
    static constexpr const char surface_extension[] = VK_KHR_XCB_SURFACE_EXTENSION_NAME;

    /* WindowSystem */

    struct WindowSystem {
        xcb_connection_t* connection;
        xcb_atom_t WM_PROTOCOLS;
        xcb_atom_t NET_WM_PING;
        xcb_atom_t WM_DELETE_WINDOW;
    };

    static void destroy_window_system(WindowSystem* ws) {
        // must be called even if has_error != 0
        xcb_disconnect(ws->connection);
    }

    template<size_t n>
    static void intern_atoms(WindowSystem* ws, const char (&str)[n], xcb_atom_t* atom) {
        xcb_generic_error_t* error; // TODO: use this

        xcb_intern_atom_cookie_t cookie = xcb_intern_atom(ws->connection, 0, n - 1, str);
        xcb_intern_atom_reply_t* reply = xcb_intern_atom_reply(ws->connection, cookie, &error);
        *atom = reply->atom;
    }

    template<size_t n, typename ...Atoms>
    static void intern_atoms(WindowSystem* ws, const char (&str)[n], xcb_atom_t* atom, Atoms&& ...atoms) {
        xcb_generic_error_t* error; // TODO: use this

        xcb_intern_atom_cookie_t cookie = xcb_intern_atom(ws->connection, 0, n - 1, str);
        intern_atoms(ws, static_cast<Atoms&&>(atoms)...);
        xcb_intern_atom_reply_t* reply = xcb_intern_atom_reply(ws->connection, cookie, &error);
        *atom = reply->atom;
    }

    DECLARE_STEPPED(void, init_window_system, WindowSystem* ws) {
        ws->connection = xcb_connect(nullptr, nullptr);
        if(xcb_connection_has_error(ws->connection) != 0) {
            destroy_window_system(ws);
            THROW();
        }

        intern_atoms(ws,
            "WM_PROTOCOLS", &ws->WM_PROTOCOLS,
            "WM_DELETE_WINDOW", &ws->WM_DELETE_WINDOW,
            "NET_WM_PING", &ws->NET_WM_PING);
    }

    /* Window */

    struct Window {
        xcb_window_t handle;
    };

    static void get_window_size(
        WindowSystem* ws,
        Window* window,
        int32_t* width,
        int32_t* height
    ) {
        xcb_generic_error_t* error; // TODO: use it

        xcb_get_geometry_cookie_t get_geometry_cookie = xcb_get_geometry(ws->connection, window->handle);
        xcb_get_geometry_reply_t* get_geometry_reply = xcb_get_geometry_reply(ws->connection, get_geometry_cookie, &error);

        *width = get_geometry_reply->width;
        *height = get_geometry_reply->height;

        free(get_geometry_reply);
    }

    /* DialogWindow (in X MainWindow is a special case of DialogWindow) */

    using DialogWindow = Window;

    static void destroy_dialog_window(WindowSystem* ws, DialogWindow* window) {
        xcb_destroy_window(ws->connection, window->handle);
    }

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    DECLARE_STEPPED(void, init_dialog_window,
        WindowSystem* ws,
        DialogWindow* window,
        Window* parent,
        int32_t width,
        int32_t height
    ) {
#pragma GCC diagnostic pop
        window->handle = xcb_generate_id(ws->connection);

        uint32_t value_mask = XCB_CW_BACK_PIXMAP | XCB_CW_EVENT_MASK;
        uint32_t value_list[] = {
            XCB_BACK_PIXMAP_NONE,

            // keyboard
            XCB_EVENT_MASK_KEY_PRESS
                // mouse
                | XCB_EVENT_MASK_BUTTON_PRESS

//                // window that covers/uncovers
//                | XCB_EVENT_MASK_EXPOSURE
//                // resize
//                | XCB_EVENT_MASK_RESIZE_REDIRECT
        };

        xcb_create_window(ws->connection,
            XCB_COPY_FROM_PARENT, window->handle, parent->handle,
            0, 0,
            width, height, 0,
            XCB_WINDOW_CLASS_INPUT_OUTPUT, XCB_COPY_FROM_PARENT,
            value_mask, value_list);

        xcb_map_window(ws->connection, window->handle);
        xcb_flush(ws->connection);
    }

    /* MainWindow (as a DialogWindow special case) */

    using MainWindow = Window;

    static void destroy_main_window(WindowSystem* ws, MainWindow* window) {
        destroy_dialog_window(ws, window);
    }

    DECLARE_STEPPED(void, init_main_window, WindowSystem* ws, MainWindow* window) {
        const xcb_setup_t* setup = xcb_get_setup(ws->connection);
        xcb_screen_t* screen = xcb_setup_roots_iterator(setup).data;

        Window screen_root = {.handle = screen->root};

        CALL_STEPPED(init_dialog_window, rethrow, ws, window, &screen_root, screen->width_in_pixels, screen->height_in_pixels);

        xcb_atom_t protocols[] = {ws->WM_DELETE_WINDOW};
        xcb_icccm_set_wm_protocols(ws->connection, window->handle, ws->WM_PROTOCOLS, STACK_ARRAY_LEN(protocols), protocols);

        return;
rethrow:
        THROW();
    }

    /* Vulkan integration */

    static VkResult create_vk_surface(
        VkSurfaceKHR* vk_surface,
        VkInstance vk_instance,
        WindowSystem* ws,
        Window* window
    ) {
        VkXcbSurfaceCreateInfoKHR create_info = {
            .sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR,
            .connection = ws->connection,
            .window = window->handle,
        };

        return vkCreateXcbSurfaceKHR(vk_instance, &create_info, nullptr, vk_surface);
    }

    /* Event loop */

    static bool handle_events(WindowSystem* ws) {
        xcb_generic_event_t* e;
        bool should_close = false;

        // TODO: should receive some kind of event struct describing responses to key sequences

        // TODO: implement other icccm protocol parts
        // https://tronche.com/gui/x/icccm/sec-4.html

        // TODO: other events?
        // https://github.com/glfw/glfw/blob/master/src/x11_window.c

        while(true) {
            e = xcb_poll_for_event(ws->connection);
            if(e == nullptr) {
                break;
            }

            // 0x80 -> event was generated by SendEvent
            switch(e->response_type & ~0x80) {
            case XCB_BUTTON_PRESS: {
//                auto event = reinterpret_cast<xcb_button_press_event_t*>(e);
//                switch(event->detail) {
//
//                }

                break;
            }
            case XCB_CLIENT_MESSAGE: {
                auto event = reinterpret_cast<xcb_client_message_event_t*>(e);
                if(event->type == ws->WM_PROTOCOLS) {
                    if(event->data.data32[0] == ws->NET_WM_PING) {

                    } else if (event->data.data32[0] == ws->WM_DELETE_WINDOW) {
                        should_close = true;
                    }
                }

                break;
            }
            }

            free(e);
        }

        xcb_flush(ws->connection);
        return should_close;
    }
};

} // namespace tide::platform
