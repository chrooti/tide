#pragma once

#include "vulkan_context.h"
#include "vulkan_buffer.h"

namespace tide::vulkan {

struct Window {
    constexpr static size_t max_inflight_frames = 2;

    VkSurfaceKHR surface;
    VkSwapchainKHR swapchain;

    uint32_t images_len;
    VkImage* images;

    // len = images_len
    VkImageView* image_views;

    // len = images_len
    VkFramebuffer* framebuffers;

    VkCommandPool command_pool;

    // len = images_len
    VkCommandBuffer* command_buffers;

    size_t current_frame;
    VkSemaphore image_available_semaphores[max_inflight_frames];
    VkSemaphore render_finished_semaphores[max_inflight_frames];
    VkFence inflight_frame_fences[max_inflight_frames];
    VkFence* inflight_image_fences;

    Buffer vertex_buffer;
};

/* Vulkan wrappers */

VkResult create_swapchain(Window* window, Context* ctx, VkExtent2D* extent, VkSurfaceCapabilitiesKHR* surface_capabilities, VkPresentModeKHR present_mode) {
    QueueFamilyIndices* queue_family_indices = &ctx->queue_family_indices;

    VkSwapchainCreateInfoKHR create_info = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = window->surface,
        .minImageCount = window->images_len,
        .imageFormat = ctx->surface_format.format,
        .imageColorSpace = ctx->surface_format.colorSpace,
        .imageExtent = *extent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .preTransform = surface_capabilities->currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = present_mode,
        .clipped = VK_TRUE,
        .oldSwapchain = VK_NULL_HANDLE,
    };

    uint32_t queue_family_indices_arr[] = {
        queue_family_indices->graphic.value,
        queue_family_indices->present.value,
    };

    if (queue_family_indices->graphic.value != queue_family_indices->present.value) {
        create_info.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        create_info.queueFamilyIndexCount = STACK_ARRAY_LEN(queue_family_indices_arr);
        create_info.pQueueFamilyIndices = queue_family_indices_arr;
    } else {
        create_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        create_info.queueFamilyIndexCount = 0;
        create_info.pQueueFamilyIndices = nullptr;
    }

    return vkCreateSwapchainKHR(ctx->device, &create_info, nullptr, &window->swapchain);
}

VkResult create_image_view(VkImageView* image_view, Context* ctx, VkImage image) {
    VkImageViewCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = ctx->surface_format.format,
        .components = {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    return vkCreateImageView(ctx->device, &create_info, nullptr, image_view);
}

VkResult create_framebuffer(VkFramebuffer* framebuffer, Context* ctx, VkImageView image_view, uint32_t width, uint32_t height) {
    VkFramebufferCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .renderPass = ctx->render_pass,
        .attachmentCount = 1,
        .pAttachments = &image_view,
        .width = width,
        .height = height,
        .layers = 1,
    };

    return vkCreateFramebuffer(ctx->device, &create_info, nullptr, framebuffer);
}

VkResult create_command_pool(Window* window, Context* ctx) {
    VkCommandPoolCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .queueFamilyIndex = ctx->queue_family_indices.graphic.value,
    };

    return vkCreateCommandPool(ctx->device, &create_info, nullptr, &window->command_pool);
}

VkResult allocate_command_buffers(Window* window, Context* ctx) {
    VkCommandBufferAllocateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .commandPool = window->command_pool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = window->images_len,
    };

    return vkAllocateCommandBuffers(ctx->device, &create_info, window->command_buffers);
}

VkResult create_semaphore(VkSemaphore* semaphore, Context* ctx) {
    VkSemaphoreCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
    };

    return vkCreateSemaphore(ctx->device, &create_info, nullptr, semaphore);
}

VkResult create_fence(VkFence* fence, Context* ctx) {
    VkFenceCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .flags = VK_FENCE_CREATE_SIGNALED_BIT,
    };

    return vkCreateFence(ctx->device, &create_info, nullptr, fence);
}

VkResult queue_submit(Window* window, Context* ctx, uint32_t image_idx) {
    VkSemaphoreSubmitInfoKHR wait_semaphore_infos[] = {
        {
            .sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR,
            .semaphore = window->image_available_semaphores[window->current_frame],
            .stageMask = VK_PIPELINE_STAGE_2_COLOR_ATTACHMENT_OUTPUT_BIT_KHR,
            .deviceIndex = 0,
        },
    };

    VkCommandBufferSubmitInfoKHR command_buffer_infos[] = {
        {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO_KHR,
            .commandBuffer = window->command_buffers[image_idx],
            .deviceMask = 0,
        },
    };
    VkSemaphoreSubmitInfoKHR signal_semaphore_infos[] = {
        {
            .sType = VK_STRUCTURE_TYPE_SEMAPHORE_SUBMIT_INFO_KHR,
            .semaphore = window->render_finished_semaphores[window->current_frame],
            .stageMask = VK_PIPELINE_STAGE_2_VERTEX_SHADER_BIT_KHR,
            .deviceIndex = 0,
        },
    };

    VkSubmitInfo2KHR submit_info = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2_KHR,
        .waitSemaphoreInfoCount = STACK_ARRAY_LEN(wait_semaphore_infos),
        .pWaitSemaphoreInfos = wait_semaphore_infos,
        .commandBufferInfoCount = STACK_ARRAY_LEN(command_buffer_infos),
        .pCommandBufferInfos = command_buffer_infos,
        .signalSemaphoreInfoCount = STACK_ARRAY_LEN(signal_semaphore_infos),
        .pSignalSemaphoreInfos = signal_semaphore_infos,
    };

    return ctx->vkQueueSubmit2KHR(ctx->graphic_queue,
        1,
        &submit_info,
        window->inflight_frame_fences[window->current_frame]);
}

VkResult queue_present(Window* window, Context* ctx, uint32_t image_idx) {
    VkSemaphore wait_semaphores[] = {
        window->render_finished_semaphores[window->current_frame],
    };
    VkSwapchainKHR swapchains[] = {
        window->swapchain,
    };

    VkPresentInfoKHR present_info = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .waitSemaphoreCount = STACK_ARRAY_LEN(wait_semaphores),
        .pWaitSemaphores = wait_semaphores,
        .swapchainCount = STACK_ARRAY_LEN(swapchains),
        .pSwapchains = swapchains,
        .pImageIndices = &image_idx,
        .pResults = nullptr,
    };

    return vkQueuePresentKHR(ctx->present_queue, &present_info);
}

} // namespace tide::vulkan

namespace tide::vulkan::window {

/* Surface init/destroy */

void destroy_surface(Window* window, Context* ctx) {
    vkDestroySurfaceKHR(ctx->instance, window->surface, nullptr);
}

template<typename Platform, typename Logger>
DECLARE_STEPPED(void, init_surface, Window* window, Logger* logger, Context* ctx, typename Platform::WindowSystem* ws, typename Platform::Window* plat_window) {
    VkResult res = Platform::create_vk_surface(&window->surface, ctx->instance, ws, plat_window);
    if (res != VK_SUCCESS) {
        vulkan_errors::log(logger, "createVkSurface*KHR", res);
        THROW();
    }
}

/* Swapchain init/destroy */

enum struct SwapchainStage {
    SWAPCHAIN,
    IMAGE_VIEWS,
    FRAMEBUFFERS,
    COMMAND_POOL,
    LAST,
};

void destroy_swapchain(Window* window, Context* ctx, SwapchainStage stage = SwapchainStage::LAST) {
    VkDevice device = ctx->device;

    switch(stage) {
    case SwapchainStage::LAST:
    case SwapchainStage::COMMAND_POOL:
        // deallocates command buffers too
        vkDestroyCommandPool(device, window->command_pool, nullptr);
        free(window->command_buffers);
    case SwapchainStage::FRAMEBUFFERS:
        for (size_t i = 0; i < window->images_len; i++) {
            vkDestroyFramebuffer(device, window->framebuffers[i], nullptr);
        }
        free(window->framebuffers);
    case SwapchainStage::IMAGE_VIEWS:
        for (size_t i = 0; i < window->images_len; i++) {
            vkDestroyImageView(device, window->image_views[i], nullptr);
        }
        free(window->image_views);
        free(window->images);
    case SwapchainStage::SWAPCHAIN:
        vkDestroySwapchainKHR(device, window->swapchain, nullptr);
    }
}

template<typename Platform, typename Logger>
DECLARE_STEPPED(void, init_stage_swapchain,
    Window* window,
    Logger* logger,
    Context* ctx,
    typename Platform::WindowSystem* ws,
    typename Platform::Window* plat_window,
    VkExtent2D* extent
) {
    VkResult res;

    VkSurfaceCapabilitiesKHR surface_capabilities;

    {
        res = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(ctx->physical_device, window->surface, &surface_capabilities);
        if (res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR", res);
            THROW();
        }

        if (surface_capabilities.currentExtent.width != UINT32_MAX) {
            *extent = surface_capabilities.currentExtent;
        } else {
            Platform::get_window_size(ws, plat_window, reinterpret_cast<int32_t*>(&extent->width), reinterpret_cast<int32_t*>(&extent->height));

            extent->width = utils::clamp(extent->width, surface_capabilities.minImageExtent.width, surface_capabilities.maxImageExtent.width);
            extent->height = utils::clamp(extent->height, surface_capabilities.minImageExtent.height, surface_capabilities.maxImageExtent.height);
        }

        uint32_t swapchain_max_image_count = surface_capabilities.maxImageCount;
        window->images_len = surface_capabilities.minImageCount + 1;
        if (window->images_len > swapchain_max_image_count && swapchain_max_image_count != 0) {
            window->images_len = swapchain_max_image_count;
        }
    }

    /* Determine the surface present mode */

    VkPresentModeKHR present_mode;

    {
        uint32_t present_modes_len = 0;
        res = vkGetPhysicalDeviceSurfacePresentModesKHR(ctx->physical_device, window->surface, &present_modes_len, nullptr);
        if (res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkGetPhysicalDeviceSurfacePresentModesKHR", res);
            THROW();
        }

        auto present_modes = MALLOC_ARRAY(VkPresentModeKHR, present_modes_len);
        res = vkGetPhysicalDeviceSurfacePresentModesKHR(ctx->physical_device, window->surface, &present_modes_len, present_modes);
        if (res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkGetPhysicalDeviceSurfacePresentModesKHR", res);
            THROW();
        }

        present_mode = VK_PRESENT_MODE_FIFO_KHR;

        for (size_t i = 0; i < present_modes_len; i++) {
            VkPresentModeKHR curr_present_mode = present_modes[i];

            if (curr_present_mode == VK_PRESENT_MODE_MAILBOX_KHR) {
                present_mode = curr_present_mode;
                break;
            }
        }

        free(present_modes);
    }

    /* Create the swapchain and initialize the views */

    {
        res = create_swapchain(window, ctx, extent, &surface_capabilities, present_mode);
        if (res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkCreateSwapchainKHR", res);
            THROW();
        }
    }
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_image_views, Window* window, Logger* logger, Context* ctx) {
    VkResult res;

    res = vkGetSwapchainImagesKHR(ctx->device, window->swapchain, &window->images_len, nullptr);
    if (res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkGetSwapchainImagesKHR", res);
        destroy_swapchain(window, ctx, SwapchainStage::SWAPCHAIN);
        THROW();
    }

    window->images = MALLOC_ARRAY(VkImage, window->images_len);
    res = vkGetSwapchainImagesKHR(ctx->device, window->swapchain, &window->images_len, window->images);
    if (res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkGetSwapchainImagesKHR", res);
        destroy_swapchain(window, ctx, SwapchainStage::SWAPCHAIN);
        THROW();
    }

    window->image_views = MALLOC_ARRAY(VkImageView, window->images_len);
    for (size_t i = 0; i < window->images_len; i++) {
        res = create_image_view(&window->image_views[i], ctx, window->images[i]);
        if (res != VK_SUCCESS) {
            for (size_t j = 0; j < i; j++) {
                vkDestroyImageView(ctx->device, window->image_views[i], nullptr);
            }
            free(window->image_views);
            free(window->images);

            vulkan_errors::log(logger, "vkCreateImageView", res);
            destroy_swapchain(window, ctx, SwapchainStage::SWAPCHAIN);
            THROW();
        }
    }
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_framebuffers, Window* window, Logger* logger, Context* ctx, VkExtent2D* extent) {
    window->framebuffers = MALLOC_ARRAY(VkFramebuffer, window->images_len);

    for (uint32_t i = 0; i < window->images_len; i++) {
        VkResult res = create_framebuffer(&window->framebuffers[i], ctx, window->image_views[i], extent->width, extent->height);
        if (res != VK_SUCCESS) {
            for (size_t j = 0; j < i; j++) {
                vkDestroyFramebuffer(ctx->device, window->framebuffers[i], nullptr);
            }
            free(window->framebuffers);

            vulkan_errors::log(logger, "vkCreateFramebuffer", res);
            destroy_swapchain(window, ctx, SwapchainStage::IMAGE_VIEWS);
            THROW();
        }
    }
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_command_pool, Window* window, Logger* logger, Context* ctx) {
    VkResult res = create_command_pool(window, ctx);
    if (res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkCreateCommandPool", res);
        destroy_swapchain(window, ctx, SwapchainStage::FRAMEBUFFERS);
        THROW();
    }
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_command_buffers, Window* window, Logger* logger, Context* ctx, VkExtent2D* extent) {
    window->command_buffers = MALLOC_ARRAY(VkCommandBuffer, window->images_len);

    VkResult res = allocate_command_buffers(window, ctx);
    if (res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkAllocateCommandBuffers", res);
        destroy_swapchain(window, ctx, SwapchainStage::COMMAND_POOL);
        THROW();
    }

    VkViewport viewport = {
        .x = 0.0F,
        .y = 0.0F,
        .width = static_cast<float>(extent->width),
        .height = static_cast<float>(extent->height),
        .minDepth = 0.0F,
        .maxDepth = 1.0F,
    };

    VkRect2D scissor = {
        .offset = {0, 0},
        .extent = *extent,
    };

    for (uint32_t i = 0; i < window->images_len; i++) {
        VkCommandBufferBeginInfo begin_info = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        };

        res = vkBeginCommandBuffer(window->command_buffers[i], &begin_info);
        if (res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkBeginCommandBuffer", res);
            destroy_swapchain(window, ctx, SwapchainStage::COMMAND_POOL);
            THROW();
        }

        VkClearValue clear_color = {
            .color = {
                .float32 = {0.0F, 0.0F, 0.0F, 1.0F},
            },
        };

        VkRenderPassBeginInfo render_pass_begin_info = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .renderPass = ctx->render_pass,
            .framebuffer = window->framebuffers[i],
            .renderArea = scissor,
            .clearValueCount = 1,
            .pClearValues = &clear_color,
        };

        vkCmdBeginRenderPass(window->command_buffers[i], &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdSetViewport(window->command_buffers[i], 0, 1, &viewport);
        vkCmdSetScissor(window->command_buffers[i], 0, 1, &scissor);
        vkCmdBindPipeline(window->command_buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, ctx->graphics_pipeline);

        VkBuffer vertex_buffers[] = {window->vertex_buffer.handle};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(window->command_buffers[i],
            0,
            STACK_ARRAY_LEN(vertex_buffers),
            vertex_buffers,
            offsets);


        vkCmdDraw(window->command_buffers[i], 3, 1, 0, 0);

        vkCmdEndRenderPass(window->command_buffers[i]);

        res = vkEndCommandBuffer(window->command_buffers[i]);
        if (res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkEndCommandBuffer", res);
            destroy_swapchain(window, ctx, SwapchainStage::COMMAND_POOL);
            THROW();
        }
    }
}

template<typename Platform, typename Logger>
DECLARE_STEPPED(void, init_swapchain, Window* window, Logger* logger, Context* ctx, typename Platform::WindowSystem* ws, typename Platform::Window* plat_window) {
    VkExtent2D extent;

    CALL_STEPPED(init_stage_swapchain<Platform>, rethrow, window, logger, ctx, ws, plat_window, &extent);
    CALL_STEPPED(init_stage_image_views, rethrow, window, logger, ctx);
    CALL_STEPPED(init_stage_framebuffers, rethrow, window, logger, ctx, &extent);
    CALL_STEPPED(init_stage_command_pool, rethrow, window, logger, ctx);
    CALL_STEPPED(init_stage_command_buffers, rethrow, window, logger, ctx, &extent);

    return;
rethrow:
    THROW();
}

template<typename Platform, typename Logger>
DECLARE_STEPPED(void, recreate_swapchain, Window* window, Logger* logger, Context* ctx, typename Platform::WindowSystem* ws, typename Platform::Window* plat_window) {
    vkDeviceWaitIdle(ctx->device);
    destroy_swapchain(window, ctx);
    CALL_STEPPED(init_swapchain<Platform>, rethrow, window, logger, ctx, ws, plat_window);

    return;
rethrow:
    THROW();
}

/* Rest (including swapchain) init/destroy */

enum struct RestStage {
    VERTEX_BUFFER,
    VERTEX_BUFFER_BINDING,
    SWAPCHAIN,
    SYNC_PRIMITIVES,
    LAST,
};

void destroy_sync_primitives(Window* window, Context* ctx, size_t idx) {
    for(size_t i = 0; i < idx; i++) {
        vkDestroyFence(ctx->device, window->inflight_frame_fences[i], nullptr);
        vkDestroySemaphore(ctx->device, window->render_finished_semaphores[i], nullptr);
        vkDestroySemaphore(ctx->device, window->image_available_semaphores[i], nullptr);
    }
    free(window->inflight_image_fences);
}


void destroy_rest(Window* window, Context* ctx, RestStage stage = RestStage::LAST) {
    switch (stage) {
    case RestStage::LAST:
    case RestStage::SYNC_PRIMITIVES:
        destroy_sync_primitives(window, ctx, Window::max_inflight_frames);
    case RestStage::SWAPCHAIN:
        destroy_swapchain(window, ctx);
    case RestStage::VERTEX_BUFFER_BINDING:
    case RestStage::VERTEX_BUFFER:
        buffer::destroy(&window->vertex_buffer, ctx);
    }
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_vertex_buffer, Window* window, Logger* logger, Context* ctx) {
    CALL_STEPPED(buffer::create, rethrow,
        &window->vertex_buffer,
        logger,
        ctx,
        sizeof(Context::vertices),
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT
    );

    asm goto("" :::: rethrow);
    return;

rethrow:
    Logger::log(logger, "Vertex buffer allocation failed");
    THROW();
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_vertex_buffer_binding, Window* window, Logger* logger, Context* ctx) {
    VkResult res;

    void* data;
    res = vkMapMemory(ctx->device,
        window->vertex_buffer.memory,
        0,
        sizeof(Context::vertices),
        0,
        &data);
    if(res != VK_SUCCESS){
        vulkan_errors::log(logger, "vkMapMemory", res);
        destroy_rest(window, ctx, RestStage::VERTEX_BUFFER);
        THROW();
    }

    memcpy(data, ctx->vertices, sizeof(Context::vertices));

    vkUnmapMemory(ctx->device, window->vertex_buffer.memory);
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_sync_primitives, Window* window, Logger* logger, Context* ctx) {
    VkResult res;

    window->inflight_image_fences = MALLOC_ARRAY(VkFence, window->images_len);
    for(size_t i = 0; i < window->images_len; i++) {
        window->inflight_image_fences[i] = VK_NULL_HANDLE;
    }

    for(size_t i = 0; i < Window::max_inflight_frames; i++) {
        res = create_semaphore(&window->image_available_semaphores[i], ctx);
        if (res != VK_SUCCESS) {
            destroy_sync_primitives(window, ctx, i);

            vulkan_errors::log(logger, "vkCreateSemaphore", res);
            destroy_rest(window, ctx, RestStage::SWAPCHAIN);
            THROW();
        }

        res = create_semaphore(&window->render_finished_semaphores[i], ctx);
        if (res != VK_SUCCESS) {
            vkDestroySemaphore(ctx->device, window->image_available_semaphores[i], nullptr);
            destroy_sync_primitives(window, ctx, i);

            vulkan_errors::log(logger, "vkCreateSemaphore", res);
            destroy_rest(window, ctx, RestStage::SWAPCHAIN);
            THROW();
        }

        res = create_fence(&window->inflight_frame_fences[i], ctx);
        if(res != VK_SUCCESS) {
            vkDestroySemaphore(ctx->device, window->image_available_semaphores[i], nullptr);
            vkDestroySemaphore(ctx->device, window->render_finished_semaphores[i], nullptr);
            destroy_sync_primitives(window, ctx, i);

            vulkan_errors::log(logger, "vkCreateFence", res);
            destroy_rest(window, ctx, RestStage::SWAPCHAIN);
            THROW();
        }
    }
}

template<typename Platform, typename Logger>
DECLARE_STEPPED(void, init_rest, Window* window, Logger* logger, Context* ctx, typename Platform::WindowSystem* ws, typename Platform::Window* plat_window) {
    window->current_frame = 0;

    CALL_STEPPED(init_stage_vertex_buffer, rethrow, window, logger, ctx);
    CALL_STEPPED(init_stage_vertex_buffer_binding, rethrow, window, logger, ctx);
    CALL_STEPPED(init_swapchain<Platform>, destroy_after_swapchain, window, logger, ctx, ws, plat_window);
    CALL_STEPPED(init_stage_sync_primitives, rethrow, window, logger, ctx);

    asm goto("" :::: destroy_after_swapchain);
    return;

destroy_after_swapchain:
    destroy_rest(window, ctx, RestStage::VERTEX_BUFFER_BINDING);
    THROW();

rethrow:
    THROW();
}

/* Loop */

// TODO: handle resize explicitly and minimzation (https://vulkan-tutorial.com/Drawing_a_triangle/Swap_chain_recreation)

template<typename Platform, typename Logger>
DECLARE_STEPPED(void, draw_frame, Window* window, Logger* logger, Context* ctx, typename Platform::WindowSystem* ws, typename Platform::Window* plat_window) {
    VkResult res;

    // wait until the current frame has been presented
    res = vkWaitForFences(ctx->device,
        1,
        &window->inflight_frame_fences[window->current_frame],
        VK_TRUE,
        UINT64_MAX);
    if(res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkWaitForFences", res);
        THROW();
    }

    // acquire the image
    uint32_t image_idx;
    while(true) {
        res = vkAcquireNextImageKHR(
            ctx->device,
            window->swapchain,
            UINT64_MAX,
            window->image_available_semaphores[window->current_frame],
            VK_NULL_HANDLE,
            &image_idx);

        if(res == VK_SUCCESS) {
            break;
        } else if(res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR) {
            // if the window has been resized rebuild the swapchain and retry
            CALL_STEPPED(recreate_swapchain<Platform>, rethrow, window, logger, ctx, ws, plat_window);
            continue;
        } else {
            vulkan_errors::log(logger, "vkAcquireNextImageKHR", res);
            THROW();
        }
    }

    // if the max_inlfight_frames is higher than the swapchain images (very rare!)
    // we could risk rendering to an image that still hasn't been presented
    if(window->inflight_image_fences[image_idx] != VK_NULL_HANDLE) {
        vkWaitForFences(ctx->device,
            1,
            &window->inflight_image_fences[image_idx],
            VK_TRUE,
            UINT64_MAX);
    }
    window->inflight_image_fences[image_idx] = window->inflight_frame_fences[window->current_frame];

    res = vkResetFences(ctx->device, 1, &window->inflight_frame_fences[window->current_frame]);
    if(res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkResetFences", res);
        THROW();
    }

    res = queue_submit(window, ctx, image_idx);
    if(res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkQueueSubmit", res);
        THROW();
    }

    res = queue_present(window, ctx, image_idx);
    if(res != VK_SUCCESS) {
        if(res == VK_ERROR_OUT_OF_DATE_KHR || res == VK_SUBOPTIMAL_KHR) {
            // the swapchain can go out of date here, too
            // in this case we don't repeat the present call since it's kind of a success
            CALL_STEPPED(recreate_swapchain<Platform>, rethrow, window, logger, ctx, ws, plat_window);
        } else {
            vulkan_errors::log(logger, "vkQueuePresent", res);
            THROW();
        }
    }

    window->current_frame = (window->current_frame + 1) % Window::max_inflight_frames;

    asm goto("" :::: rethrow);
    return;

rethrow:
    THROW();
}

} // namespace tide::vulkan::window