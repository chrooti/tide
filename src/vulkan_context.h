#pragma once

#include <string.h>

#include <glm/glm.hpp>

#define INCBIN_PREFIX
#define INCBIN_STYLE INCBIN_STYLE_SNAKE
#include <incbin/incbin.h>

#include "macros.h"
#include "utils.h"
#include "vulkan.h"
#include "vulkan_errors.h"

namespace tide::vulkan {

/* Data types */

struct Vertex {
    glm::vec2 pos;
    glm::vec3 color;
};

struct QueueFamilyIndices {
    utils::Optional<uint32_t> graphic;
    utils::Optional<uint32_t> present;
};

struct Context {
    VkInstance instance;
    VkPhysicalDevice physical_device;
    QueueFamilyIndices queue_family_indices;
    VkDevice device;
    VkQueue graphic_queue;
    VkQueue present_queue;
    VkSurfaceFormatKHR surface_format;
    VkRenderPass render_pass;
    VkPipelineLayout graphics_pipeline_layout;
    VkPipeline graphics_pipeline;

    PFN_vkQueueSubmit2KHR vkQueueSubmit2KHR;

    Vertex vertices[3];
};

/* Compiled shaders */

INCBIN(frag_shader, "shaders/frag.spv");
INCBIN(vert_shader, "shaders/vert.spv");

/* Vulkan wrappers */

template<const char surface_ext[]>
VkResult create_instance(Context* ctx, bool is_debug_enabled) {
    VkApplicationInfo app_info = {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = "tide",
        .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
        .pEngineName = "tide_engine",
        .engineVersion = VK_MAKE_VERSION(1, 0, 0),
        .apiVersion = VK_API_VERSION_1_2
    };

    const char* const extensions[] = {
        VK_KHR_SURFACE_EXTENSION_NAME,
        surface_ext,
    };

    VkInstanceCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &app_info,
        .enabledLayerCount = 0,
        .enabledExtensionCount = STACK_ARRAY_LEN(extensions),
        .ppEnabledExtensionNames = extensions,
    };

    const char* validation_layers[] = {
        "VK_LAYER_KHRONOS_validation"
    };
    if(is_debug_enabled) {
        create_info.enabledLayerCount = STACK_ARRAY_LEN(validation_layers);
        create_info.ppEnabledLayerNames = validation_layers;
    }

    return vkCreateInstance(&create_info, nullptr, &ctx->instance);
}

VkResult create_device(Context* ctx) {
    uint32_t queue_info_len = 0;
    VkDeviceQueueCreateInfo queue_info[2];

    float queue_priority = 1.0;
    queue_info[queue_info_len] = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .queueFamilyIndex = ctx->queue_family_indices.graphic.value,
        .queueCount = 1,
        .pQueuePriorities = &queue_priority
    };
    queue_info_len++;

    if(ctx->queue_family_indices.graphic.value != ctx->queue_family_indices.present.value) {
        queue_info[queue_info_len] = {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = ctx->queue_family_indices.present.value,
            .queueCount = 1,
            .pQueuePriorities = &queue_priority
        };
        queue_info_len++;
    }

    VkPhysicalDeviceFeatures device_features{};

    const char* const vk_device_extensions[] = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        VK_KHR_SYNCHRONIZATION_2_EXTENSION_NAME,
    };

    // TODO: use feature detection (vkGetPhysicalDeviceFeatures2)
    VkPhysicalDeviceSynchronization2FeaturesKHR sync2 = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES_KHR,
        .synchronization2 = VK_TRUE,
    };

    VkDeviceCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = &sync2,
        .queueCreateInfoCount = queue_info_len,
        .pQueueCreateInfos = queue_info,
        .enabledLayerCount = 0,
        .enabledExtensionCount = STACK_ARRAY_LEN(vk_device_extensions),
        .ppEnabledExtensionNames = vk_device_extensions,
        .pEnabledFeatures = &device_features
    };

    return vkCreateDevice(ctx->physical_device, &create_info, nullptr, &ctx->device);
}

VkResult create_shader_module(
    Context* ctx,
    VkShaderModule* module,
    const void* code,
    size_t code_size
) {
    VkShaderModuleCreateInfo create_info = {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = code_size,
        .pCode = reinterpret_cast<const uint32_t*>(code),
    };

    return vkCreateShaderModule(ctx->device, &create_info, nullptr, module);
}

VkResult create_render_pass(Context* ctx) {
    // TODO: review this

    VkAttachmentDescription2 attachment_description = {
        .sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2,
        .format = ctx->surface_format.format,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
        .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
    };

    VkAttachmentReference2 attachment_reference = {
        .sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2,
        .attachment = 0,
        .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
    };

    VkSubpassDescription2 subpass_description = {
        .sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2,
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = 1,
        .pColorAttachments = &attachment_reference,
    };

    VkMemoryBarrier2KHR memory_barrier = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2_KHR,
        .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .srcAccessMask = 0,
        .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
    };

    VkSubpassDependency2 dependency = {
        .sType = VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2,
        .pNext = &memory_barrier,
        .srcSubpass = VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
    };

    VkRenderPassCreateInfo2 render_pass_create_info = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2,
        .attachmentCount = 1,
        .pAttachments = &attachment_description,
        .subpassCount = 1,
        .pSubpasses = &subpass_description,
        .dependencyCount = 1,
        .pDependencies = &dependency,
    };

    return vkCreateRenderPass2(ctx->device, &render_pass_create_info, nullptr, &ctx->render_pass);
}

VkResult create_pipeline_layout(Context* ctx) {
    VkPipelineLayoutCreateInfo pipeline_layout_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = 0,
        .pSetLayouts = nullptr,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = nullptr,
    };

    return vkCreatePipelineLayout(ctx->device, &pipeline_layout_create_info, nullptr, &ctx->graphics_pipeline_layout);
}

VkResult create_pipeline(Context* ctx, VkShaderModule vert_shader_module, VkShaderModule frag_shader_module) {
    VkPipelineShaderStageCreateInfo shader_stages[] = {
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .module = vert_shader_module,
            .pName = "main",
        },
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = frag_shader_module,
            .pName = "main",
        },
    };

    VkDynamicState dynamic_states[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo pipeline_dynamic_state_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = STACK_ARRAY_LEN(dynamic_states),
        .pDynamicStates = dynamic_states
    };

    VkVertexInputBindingDescription binding_descriptions[] = {
        {
            .binding = 0,
            .stride = sizeof(Vertex),
            .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        }
    };

    VkVertexInputAttributeDescription attribute_descriptions[] = {
        {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32_SFLOAT,
            .offset = offsetof(Vertex, pos)
        },
        {
            .location = 1,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32_SFLOAT,
            .offset = offsetof(Vertex, color)
        }
    };

    VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = STACK_ARRAY_LEN(binding_descriptions),
        .pVertexBindingDescriptions = binding_descriptions,
        .vertexAttributeDescriptionCount = STACK_ARRAY_LEN(attribute_descriptions),
        .pVertexAttributeDescriptions = attribute_descriptions,
    };

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        // TODO check if alternatives are more performant
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkPipelineViewportStateCreateInfo viewport_state_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .pViewports = nullptr,
        .scissorCount = 1,
        .pScissors = nullptr,
    };

    VkPipelineRasterizationStateCreateInfo rasterization_state_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_BACK_BIT,
        .frontFace = VK_FRONT_FACE_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .lineWidth = 1.0F,
    };

    VkPipelineMultisampleStateCreateInfo multisample_state_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
        .sampleShadingEnable = VK_FALSE,
    };

    VkPipelineColorBlendAttachmentState color_blend_attachment_states[] = {
        {
            .blendEnable = VK_FALSE,
            .colorWriteMask = VK_COLOR_COMPONENT_R_BIT
                | VK_COLOR_COMPONENT_G_BIT
                | VK_COLOR_COMPONENT_B_BIT
                | VK_COLOR_COMPONENT_A_BIT,
        }
    };

    VkPipelineColorBlendStateCreateInfo color_blend_state_create_info = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .attachmentCount = STACK_ARRAY_LEN(color_blend_attachment_states),
        .pAttachments = color_blend_attachment_states,
    };

    VkGraphicsPipelineCreateInfo graphics_pipeline_create_info = {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = STACK_ARRAY_LEN(shader_stages),
        .pStages = shader_stages,
        .pVertexInputState = &vertex_input_state_create_info,
        .pInputAssemblyState = &input_assembly_state_create_info,
        .pViewportState = &viewport_state_create_info,
        .pRasterizationState = &rasterization_state_create_info,
        .pMultisampleState = &multisample_state_create_info,
        .pDepthStencilState = nullptr,
        .pColorBlendState = &color_blend_state_create_info,
        .pDynamicState = &pipeline_dynamic_state_create_info,
        .layout = ctx->graphics_pipeline_layout,
        .renderPass = ctx->render_pass,
        .subpass = 0,
        .basePipelineHandle = VK_NULL_HANDLE,
        .basePipelineIndex = -1,
    };

    return vkCreateGraphicsPipelines(ctx->device, VK_NULL_HANDLE, 1, &graphics_pipeline_create_info, nullptr, &ctx->graphics_pipeline);
}

} // namespace tide::vulkan

namespace tide::vulkan::context {

/* Instance init/destroy */

void destroy_instance(Context* ctx) {
    vkDestroyInstance(ctx->instance, nullptr);
}

template<const char surface_ext[], typename Logger, typename Settings>
DECLARE_STEPPED(void, init_instance, Context* ctx, Logger* logger, Settings* settings)  {
    VkResult res = create_instance<surface_ext>(ctx, settings->vk.is_debug_enabled);
    if(res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkCreateInstance", res);
        THROW();
    }
}

/* Rest init/destroy */

enum struct RestStage {
    DEVICE,
    RENDER_PASS,
    PIPELINE_LAYOUT,
    PIPELINE,
    LAST,
};

void destroy_rest(Context* ctx, RestStage stage = RestStage::LAST) {
    switch(stage) {
    case RestStage::LAST:
    case RestStage::PIPELINE:
        vkDestroyPipeline(ctx->device, ctx->graphics_pipeline, nullptr);
    case RestStage::PIPELINE_LAYOUT:
        vkDestroyPipelineLayout(ctx->device, ctx->graphics_pipeline_layout, nullptr);
    case RestStage::RENDER_PASS:
        vkDestroyRenderPass(ctx->device, ctx->render_pass, nullptr);
    case RestStage::DEVICE:
        vkDestroyDevice(ctx->device, nullptr);
    }
}

template<typename Logger, typename Settings>
DECLARE_STEPPED(void, init_stage_device, Context* ctx, Logger* logger, Settings* settings, VkSurfaceKHR surface) {
    VkResult res;

    /* Select a physical device */

    {
        uint32_t physical_devices_len = 0;
        res = vkEnumeratePhysicalDevices(ctx->instance, &physical_devices_len, nullptr);
        if(res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkEnumeratePhysicalDevices", res);
            THROW();
        }
        if(physical_devices_len == 0) {
            Logger::log(logger, "vkEnumeratePhysicalDevices: len = 0\n");
            THROW();
        }

        auto physical_devices = MALLOC_ARRAY(VkPhysicalDevice, physical_devices_len);
        vkEnumeratePhysicalDevices(ctx->instance, &physical_devices_len, physical_devices);

        ctx->physical_device = physical_devices[0];

        for(size_t i = 0; i < physical_devices_len; i++) {
            VkPhysicalDevice curr_physical_device = physical_devices[i];

            VkPhysicalDeviceProperties physical_device_properties;
            vkGetPhysicalDeviceProperties(curr_physical_device, &physical_device_properties);

            if(memcmp(settings->vk.gpu_uuid, physical_device_properties.pipelineCacheUUID, VK_UUID_SIZE) == 0) {
                ctx->physical_device = curr_physical_device;
                break;
            }
        }

        free(physical_devices);
    }

    /* Find queue family indices */

    {
        uint32_t queue_family_properties_len = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(ctx->physical_device, &queue_family_properties_len, nullptr);

        auto queue_family_properties = MALLOC_ARRAY(VkQueueFamilyProperties, queue_family_properties_len);
        vkGetPhysicalDeviceQueueFamilyProperties(ctx->physical_device, &queue_family_properties_len, queue_family_properties);

        for(uint32_t i = 0; i < queue_family_properties_len; i++) {
            VkQueueFamilyProperties curr_queue_family_properties = queue_family_properties[i];

            if((curr_queue_family_properties.queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0) {
                utils::optional::set(&ctx->queue_family_indices.graphic, i);
            }

            VkBool32 is_present_supported = 0;
            res = vkGetPhysicalDeviceSurfaceSupportKHR(ctx->physical_device, i, surface, &is_present_supported);
            if(res != VK_SUCCESS) {
                free(queue_family_properties);
                vulkan_errors::log(logger, "vkGetPhysicalDeviceSurfaceSupportKHR", res);
                THROW();
            }
            if(is_present_supported != 0) {
                utils::optional::set(&ctx->queue_family_indices.present, i);
            }

            if(ctx->queue_family_indices.graphic.is_valid
                && ctx->queue_family_indices.present.is_valid) {
                free(queue_family_properties);
                goto queue_family_indices_found;
            }
        }

        free(queue_family_properties);
        Logger::log(logger, "Couldn't find queue indices\n");
        THROW();

queue_family_indices_found:;

    }

    /* Create the logical device */

    {
        res = create_device(ctx);
        if(res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkCreateDevice", res);
            THROW();
        }
    }

    // take a reference extension functions
    ctx->vkQueueSubmit2KHR = reinterpret_cast<PFN_vkQueueSubmit2KHR>(
        vkGetInstanceProcAddr(ctx->instance, "vkQueueSubmit2KHR")
    );

    /* Get the queues */

    {
        QueueFamilyIndices* queue_family_indices = &ctx->queue_family_indices;

        vkGetDeviceQueue(ctx->device, queue_family_indices->graphic.value, 0, &ctx->graphic_queue);

        if(queue_family_indices->graphic.value == queue_family_indices->present.value) {
            ctx->present_queue = ctx->graphic_queue;
        } else {
            vkGetDeviceQueue(ctx->device, queue_family_indices->present.value, 0, &ctx->present_queue);
        }
    }
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_render_pass, Context* ctx, Logger* logger, VkSurfaceKHR surface) {
    VkResult res;

    /* Determine the surface format */

    {
        uint32_t surface_formats_len = 0;
        res = vkGetPhysicalDeviceSurfaceFormatsKHR(ctx->physical_device, surface, &surface_formats_len, nullptr);
        if(res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkGetPhysicalDeviceSurfaceFormatsKHR", res);
            destroy_rest(ctx, RestStage::DEVICE);
            THROW();
        }

        auto surface_formats = MALLOC_ARRAY(VkSurfaceFormatKHR, surface_formats_len);
        res = vkGetPhysicalDeviceSurfaceFormatsKHR(ctx->physical_device, surface, &surface_formats_len, surface_formats);
        if(res != VK_SUCCESS) {
            free(surface_formats);
            vulkan_errors::log(logger, "vkGetPhysicalDeviceSurfaceFormatsKHR", res);
            destroy_rest(ctx, RestStage::DEVICE);
            THROW();
        }

        ctx->surface_format = surface_formats[0];

        for(size_t i = 0; i < surface_formats_len; i++) {
            VkSurfaceFormatKHR* curr_surface_format = &surface_formats[i];

            if(curr_surface_format->format == VK_FORMAT_B8G8R8A8_SRGB && curr_surface_format->colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                ctx->surface_format = *curr_surface_format;
                break;
            }
        }

        free(surface_formats);
    }

    /* Create the render pass */

    {
        res = create_render_pass(ctx);
        if(res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkCreateRenderPass", res);
            destroy_rest(ctx, RestStage::DEVICE);
            THROW();
        }
    }
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_create_pipeline_layout, Context* ctx, Logger* logger) {
    VkResult res = create_pipeline_layout(ctx);
    if(res != VK_SUCCESS) {
        vulkan_errors::log(logger, "vkCreatePipelineLayout", res);
        destroy_rest(ctx, RestStage::RENDER_PASS);
        THROW();
    }
}

template<typename Logger>
DECLARE_STEPPED(void, init_stage_create_pipeline, Context* ctx, Logger* logger) {
    VkResult res;

    /* Load the shaders */

    VkShaderModule vert_shader_module;
    VkShaderModule frag_shader_module;

    {
        res = create_shader_module(ctx, &vert_shader_module, vert_shader_data, vert_shader_size);
        if(res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkCreateShaderModule", res);
            destroy_rest(ctx, RestStage::PIPELINE_LAYOUT);
            THROW();
        }


        res = create_shader_module(ctx, &frag_shader_module, frag_shader_data, frag_shader_size);
        if(res != VK_SUCCESS) {
            vkDestroyShaderModule(ctx->device, vert_shader_module, nullptr);
            vulkan_errors::log(logger, "vkCreateShaderModule", res);
            destroy_rest(ctx, RestStage::PIPELINE_LAYOUT);
            THROW();
        }
    }

    ctx->vertices[0] = {{0.0F, -0.5F}, {1.0F, 0.0F, 0.0F}};
    ctx->vertices[1] = {{0.5F, 0.5F}, {0.0F, 1.0F, 0.0F}};
    ctx->vertices[2] = {{-0.5F, 0.5F}, {0.0F, 0.0F, 1.0F}};

    /* Create the pipeline */

    {
        res = create_pipeline(ctx, vert_shader_module, frag_shader_module);

        vkDestroyShaderModule(ctx->device, vert_shader_module, nullptr);
        vkDestroyShaderModule(ctx->device, frag_shader_module, nullptr);

        if(res != VK_SUCCESS) {
            vulkan_errors::log(logger, "vkCreateShaderModule", res);
            destroy_rest(ctx, RestStage::PIPELINE_LAYOUT);
            THROW();
        }
    }
}

template<typename Logger, typename Settings>
DECLARE_STEPPED(void, init_rest, Context* ctx, Logger* logger, Settings* settings, VkSurfaceKHR surface) {
    CALL_STEPPED(init_stage_device, rethrow, ctx, logger, settings, surface);
    CALL_STEPPED(init_stage_render_pass, rethrow, ctx, logger, surface);
    CALL_STEPPED(init_stage_create_pipeline_layout, rethrow, ctx, logger);
    CALL_STEPPED(init_stage_create_pipeline, rethrow, ctx, logger);

    return;
rethrow:
    THROW();
}

} // namespace tide::vulkan::context
