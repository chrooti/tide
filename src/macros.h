#pragma once

#define MALLOC_ARRAY(T, size) reinterpret_cast<T*>( \
    malloc(sizeof(T) * (size))    \
)

#define STACK_ARRAY_LEN(x) (sizeof(x)/sizeof(*(x)))

#define INLINE static inline __attribute__((__always_inline__, __unused__))

#define _CONCAT(a, b) a ## b
#define CONCAT(A, B) _CONCAT(A, B)

#define DECLARE_STEPPED(RET, NAME, ...) \
    INLINE RET NAME(void** __restrict __ret, void* __restrict __ret_error, ##__VA_ARGS__)

#define THROW(VAL) \
    *__ret = __ret_error; \
    return VAL;

#define _CALL_STEPPED(CNT, NAME, ERR_LABEL, ...) \
    void* CONCAT(__jump_to_, CNT) = CONCAT(&&__success_, CNT); \
    NAME(CONCAT(&__jump_to_, CNT), && ERR_LABEL, ##__VA_ARGS__); \
    CONCAT(__success_, CNT):;

#define CALL_STEPPED(NAME, ERR_LABEL, ...) \
    _CALL_STEPPED(__COUNTER__, NAME, ERR_LABEL, __VA_ARGS__)
