#pragma once

#include "io.h"

namespace tide {

struct Logger {
    utter::Output<> out;

    static void init(Logger* logger) {
        logger->out = utter::output();
    }

    template<typename ...Buffers>
    static int log(Logger* logger, Buffers&&... bufs) {
        return utter::to(logger->out, static_cast<Buffers&&>(bufs)..., utter::to_stderr);
    }

};

} // namespace tide