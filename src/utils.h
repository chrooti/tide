#pragma once

namespace tide::utils {

template<typename T>
struct Optional {
    T value;
    bool is_valid;
};

namespace optional {
    template<typename T>
    void set(Optional<T>* optional, T value) {
        optional->value = value;
        optional->is_valid = true;
    }
} // namespace optional

template<typename T>
T clamp(T num, T min, T max) {
    if(num < min) {
        return min;
    }
    if (num > max) {
        return max;
    }

    return num;
}

template<typename T>
constexpr T max_enum() {
    return static_cast<T>(static_cast<size_t>(T::__MAX_VAL) - 1);
}

} // namespace tide::utils