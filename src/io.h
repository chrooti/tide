#pragma once

#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <unistd.h>

#include <utter/utter.hpp>

namespace tide::io {

ssize_t read_file(const char* path, char** buf) {
    struct statx info;
    unsigned int wanted_mask = STATX_SIZE;
    int ret = statx(AT_FDCWD, path, 0, wanted_mask, &info);
    if(ret == -1 || (info.stx_mask & wanted_mask) != wanted_mask) {
        return -1;
    }

    *buf = reinterpret_cast<char*>(malloc(info.stx_size));

    int fd = openat(AT_FDCWD, path, 0, O_RDONLY);
    if (fd == -1) {

cleanup_after_alloc:
        free(*buf);
        return -1;
    }

    ssize_t read_size = read(fd, *buf, info.stx_size);
    if(read_size == -1) {
        close(fd);
        goto cleanup_after_alloc;
    }


    close(fd);

    return static_cast<ssize_t>(info.stx_size);
}


} // namespace tide::io