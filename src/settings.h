#pragma once

#include <vulkan/vulkan_core.h>

namespace tide {

struct Settings {
    struct {
        bool is_debug_enabled;
        uint8_t gpu_uuid[VK_UUID_SIZE];
    } vk;
};

} // namespace tide

namespace tide::settings {

void read_settings(Settings* settings) {
    settings->vk = {
        .is_debug_enabled = true,
        // .gpu_uuid
    };
}

void write_settings(Settings* settings) {

}

} // namespace tide::settings
