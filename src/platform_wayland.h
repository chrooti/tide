/**
 * Wayland API is complete shit and people who "engineered"
 * this brain damage shouldn't be allowed anywhere near a
 * computer, I won't elaborate, I'll just put it here
*/

#pragma once

#include <string.h>

#include "macros.h"
#include "vulkan.h"

// clang-format off
// this is left C style in order to be comparable to the original
#undef wl_array_for_each
#define wl_array_for_each(pos, array) /* NOLINT */ \
	for (pos = (uint32_t*) (array)->data; /* NOLINT */ \
	     (const char *) pos < ((const char *) (array)->data + (array)->size); /* NOLINT */ \
	     (pos)++)
// clang-format on

namespace tide::platform {

struct Wayland {
    static constexpr const char surface_extension[] = VK_KHR_WAYLAND_SURFACE_EXTENSION_NAME;

    /* WindowSystem */

    struct WindowSystem {
        wl_display* display;
        wl_registry* registry;
        wl_compositor* compositor;
        xdg_wm_base* wm_base;
    };

    /* Wayland brain damage intermezzo - ehm WindowSystem integration */

    static void wm_base_handle_ping(
        void* data,
        struct xdg_wm_base* wm_base,
        uint32_t serial
    ) {
        (void) data;

        xdg_wm_base_pong(wm_base, serial);
    }

    static constexpr xdg_wm_base_listener wm_base_listener = {
        .ping = &wm_base_handle_ping,
    };

    static void registry_handle_global(
        void *data,
        wl_registry *registry,
        uint32_t id,
        const char *interface,
        uint32_t version
    ) {
        auto ws = reinterpret_cast<WindowSystem*>(data);

        if (strcmp(interface, "wl_compositor") == 0) {
            ws->compositor = reinterpret_cast<wl_compositor*>(
                wl_registry_bind(registry, id, &wl_compositor_interface, version)
            );
        } else if (strcmp(interface, "xdg_wm_base") == 0) {
            ws->wm_base = reinterpret_cast<xdg_wm_base*>(
                wl_registry_bind(registry, id, &xdg_wm_base_interface, version)
            );
            xdg_wm_base_add_listener(ws->wm_base, &wm_base_listener, nullptr);
        }
    }

    static void registry_handle_global_remove(
        void *data,
        wl_registry *registry,
        uint32_t id
    ) {
        (void) data;
        (void) registry;
        (void) id;
    }

    static constexpr wl_registry_listener registry_listener = {
        .global = &registry_handle_global,
        .global_remove = &registry_handle_global_remove,
    };

    /* WindowSystem */

    enum WindowSystemStage {
        WINDOW_SYSTEM_STAGE_DISPLAY,
        WINDOW_SYSTEM_STAGE_REGISTRY,
    };

    static void destroy_window_system_from_stage(WindowSystem* ws, WindowSystemStage stage) {
        switch(stage) {
        case WINDOW_SYSTEM_STAGE_REGISTRY:
            xdg_wm_base_destroy(ws->wm_base);
        case WINDOW_SYSTEM_STAGE_DISPLAY:
            wl_display_disconnect(ws->display);
        }
    }

    static void destroy_window_system(WindowSystem* ws) {
        destroy_window_system_from_stage(ws, WINDOW_SYSTEM_STAGE_REGISTRY);
    }

    DECLARE_STEPPED(void, init_window_system, WindowSystem* ws) {
        ws->display = wl_display_connect(nullptr);
        if(ws->display == nullptr) {
            destroy_window_system_from_stage(ws, WINDOW_SYSTEM_STAGE_DISPLAY);
            THROW();
        }

        ws->registry = wl_display_get_registry(ws->display);
        wl_registry_add_listener(ws->registry, &registry_listener, ws);

        wl_display_dispatch(ws->display);
        wl_display_roundtrip(ws->display);

        if(ws->compositor == nullptr || ws->wm_base == nullptr) {
            destroy_window_system_from_stage(ws, WINDOW_SYSTEM_STAGE_REGISTRY);
            THROW();
        }
    }

    /* Window */

    struct Window {
        wl_surface* surface;
        xdg_surface* xdg_surface;
        int32_t width;
        int32_t height;
    };

    void get_window_size(
        WindowSystem* ws,
        Window* window,
        int32_t* width,
        int32_t* height
    ) {
        (void) ws;

        *width = window->width;
        *height = window->height;
    }

    static void xdg_surface_handle_configure(
        void* data,
        struct xdg_surface* surface,
        uint32_t serial
    ) {
        (void) data;

        xdg_surface_ack_configure(surface, serial);
    }

    static constexpr xdg_surface_listener xdg_surface_listener = {
        .configure = &xdg_surface_handle_configure,
    };

    /* MainWindow */

    struct MainWindow {
        Window base;
        xdg_toplevel* toplevel;
    };

    static void xdg_toplevel_handle_configure(
        void* data,
        struct xdg_toplevel* toplevel,
        int32_t width,
        int32_t height,
        struct wl_array* states
    ) {
        (void) toplevel;

        auto window = reinterpret_cast<MainWindow*>(data);
        window->base.width = width;
        window->base.height = height;

        uint32_t* state;
        wl_array_for_each(state, states) {
            switch (*state) {
//            case XDG_TOPLEVEL_STATE_MAXIMIZED:
//                break;
//            case XDG_TOPLEVEL_STATE_FULLSCREEN:
//                break;
//            case XDG_TOPLEVEL_STATE_RESIZING:
//                break;
//            case XDG_TOPLEVEL_STATE_ACTIVATED:
//                break;
            }
        }
    }

    static void xdg_toplevel_handle_close(
        void* data,
        struct xdg_toplevel* toplevel
    ) {
        (void) data;
        (void) toplevel;
    }

    static constexpr xdg_toplevel_listener xdg_toplevel_listener = {
        .configure = &xdg_toplevel_handle_configure,
        .close = &xdg_toplevel_handle_close,
    };

    enum MainWindowStage {
        MAIN_WINDOW_STAGE_SURFACE,
        MAIN_WINDOW_STAGE_XDG_SURFACE,
        MAIN_WINDOW_STAGE_TOPLEVEL,
    };

    static void destroy_main_window_from_stage(
        WindowSystem* ws,
        MainWindow* window,
        MainWindowStage stage
    ) {
        (void) ws;

        switch(stage) {
        case MAIN_WINDOW_STAGE_TOPLEVEL:
            xdg_toplevel_destroy(window->toplevel);
        case MAIN_WINDOW_STAGE_XDG_SURFACE:
            xdg_surface_destroy(window->base.xdg_surface);
        case MAIN_WINDOW_STAGE_SURFACE:
            wl_surface_destroy(window->base.surface);
        }
    }

    static void destroy_main_window(WindowSystem* ws, MainWindow* window) {
        destroy_main_window_from_stage(ws, window, MAIN_WINDOW_STAGE_TOPLEVEL);
    }

    DECLARE_STEPPED(void, init_main_window, WindowSystem* ws, MainWindow* window) {
        window->base.surface = wl_compositor_create_surface(ws->compositor);
        if(window->base.surface == nullptr) {
            destroy_main_window_from_stage(ws, window, MAIN_WINDOW_STAGE_SURFACE);
            THROW();
        }

        window->base.xdg_surface = xdg_wm_base_get_xdg_surface(ws->wm_base, window->base.surface);
        if(window->base.xdg_surface == nullptr) {
            destroy_main_window_from_stage(ws, window, MAIN_WINDOW_STAGE_XDG_SURFACE);
            THROW();
        }

        xdg_surface_add_listener(window->base.xdg_surface, &xdg_surface_listener, window);
        window->toplevel = xdg_surface_get_toplevel(window->base.xdg_surface);
        if(window->toplevel == nullptr) {
            destroy_main_window_from_stage(ws, window, MAIN_WINDOW_STAGE_TOPLEVEL);
            THROW();
        }

        xdg_toplevel_add_listener(window->toplevel, &xdg_toplevel_listener, window);

        wl_surface_commit(window->base.surface);
        wl_display_roundtrip(ws->display);
    }

    /* DialogWindow */

    struct DialogWindow {
        Window base;
        xdg_positioner* positioner;
        xdg_popup* popup;
    };

    static void xdg_popup_handle_configure(
        void* data,
        struct xdg_popup *xdg_popup,
        int32_t x,
        int32_t y,
        int32_t width,
        int32_t height
    ) {
        (void) xdg_popup;
        (void) x;
        (void) y;

        auto dialog = reinterpret_cast<DialogWindow*>(data);
        dialog->base.width = width;
        dialog->base.height = height;
    }

    static void xdg_popup_handle_popup_done(
        void *data,
        struct xdg_popup *xdg_popup
    ) {
        (void) data;

        xdg_popup_destroy(xdg_popup);
    }

    static void xdg_popup_handle_repositioned(
        void* data,
        struct xdg_popup* xdg_popup,
        uint32_t token
    ) {
        (void) data;
        (void) xdg_popup;
        (void) token;
    }

    static constexpr xdg_popup_listener xdg_popup_listener = {
        .configure = &xdg_popup_handle_configure,
        .popup_done = &xdg_popup_handle_popup_done,
        .repositioned = &xdg_popup_handle_repositioned,
    };

    enum DialogWindowStage {
        DIALOG_WINDOW_STAGE_SURFACE,
        DIALOG_WINDOW_STAGE_XDG_SURFACE,
        DIALOG_WINDOW_STAGE_POSITIONER,
        DIALOG_WINDOW_STAGE_POPUP,
    };

    static void destroy_dialog_window_from_stage(
        WindowSystem* ws,
        DialogWindow* window,
        DialogWindowStage stage
    ) {
        (void) ws;

        switch(stage) {
        case DIALOG_WINDOW_STAGE_POPUP:
            // xdg_popup_destroy destroys the xdg_surface
            xdg_popup_destroy(window->popup);
            goto destroy_wl_surface;
        case DIALOG_WINDOW_STAGE_POSITIONER:
            xdg_positioner_destroy(window->positioner);
        case DIALOG_WINDOW_STAGE_XDG_SURFACE:
            xdg_surface_destroy(window->base.xdg_surface);

destroy_wl_surface:
        case DIALOG_WINDOW_STAGE_SURFACE:
            wl_surface_destroy(window->base.surface);
        }
    }

    static void destroy_dialog_window(WindowSystem* ws, DialogWindow* window) {
        destroy_dialog_window_from_stage(ws, window, DIALOG_WINDOW_STAGE_POPUP);
    }

    DECLARE_STEPPED(void, init_dialog_window,
        WindowSystem* ws,
        DialogWindow* window,
        Window* parent,
        int32_t width,
        int32_t height
    ) {
        window->base.surface = wl_compositor_create_surface(ws->compositor);
        if(window->base.surface == nullptr) {
            destroy_dialog_window_from_stage(ws, window, DIALOG_WINDOW_STAGE_SURFACE);
            THROW();
        }

        window->base.xdg_surface = xdg_wm_base_get_xdg_surface(ws->wm_base, window->base.surface);
        if(window->base.xdg_surface == nullptr) {
            destroy_dialog_window_from_stage(ws, window, DIALOG_WINDOW_STAGE_XDG_SURFACE);
            THROW();
        }

        window->positioner = xdg_wm_base_create_positioner(ws->wm_base);
        if(window->positioner == nullptr) {
            destroy_dialog_window_from_stage(ws, window, DIALOG_WINDOW_STAGE_POSITIONER);
            THROW();
        }

        xdg_positioner_set_size(window->positioner, width, height);
        xdg_positioner_set_anchor_rect(window->positioner, 0, 0, parent->width, parent->height);

        window->popup = xdg_surface_get_popup(window->base.xdg_surface, parent->xdg_surface, window->positioner);
        if(window->popup == nullptr) {
            destroy_dialog_window_from_stage(ws, window, DIALOG_WINDOW_STAGE_POSITIONER);
            THROW();
        }

        xdg_popup_add_listener(window->popup, &xdg_popup_listener, window);

        wl_surface_commit(window->base.surface);
        wl_display_roundtrip(ws->display);

        xdg_positioner_destroy(window->positioner);
    }

    /* Vulkan integration */

    static VkResult create_vk_surface(
        VkSurfaceKHR* vk_surface,
        VkInstance vk_instance,
        WindowSystem* ws,
        Window* window
    ) {
        VkWaylandSurfaceCreateInfoKHR create_info = {
            .sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR,
            .display = ws->display,
            .surface = window->surface,
        };

        return vkCreateWaylandSurfaceKHR(vk_instance, &create_info, nullptr, vk_surface);
    }

};

} // namespace tide::platform