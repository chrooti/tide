#!/bin/bash

set -e

shopt -s globstar
shopt -s nullglob

ROOT_DIR="$(dirname "${BASH_SOURCE[0]}")/.."

DEBUG_BUILD_DIR="$ROOT_DIR/cmake-build-debug"
RELEASE_BUILD_DIR="$ROOT_DIR/cmake-build-release"

if [[ -d $RELEASE_BUILD_DIR ]]; then
    BUILD_DIR=$RELEASE_BUILD_DIR
elif [[ -d $DEBUG_BUILD_DIR ]]; then
    BUILD_DIR=$DEBUG_BUILD_DIR
else
    echo "build directory not found"
    exit 1
fi

# running checks on all files is expensive, but
# - the codebase is small (for now)
# - I'd _really_ like to have independent headers
# - hopefully modules come soon enough
clang-tidy -p "$BUILD_DIR" "$ROOT_DIR"/src/**/*.h

# clang-tidy is not running this for some reason even with options applied
clang-format -i --style=file "$ROOT_DIR"/src/**/*.*
