#pragma once

#include <vulkan/vulkan.h>

// TODO:
//  extensions[1] = "VK_KHR_win32_surface";
//  extensions[1] = "VK_EXT_metal_surface";

// todo: revise this
// add logger api?

struct Platform {
    static constexpr const char* surface_extension;

    /* WindowSystem */

    struct WindowSystem;

    static void destroy_window_system(
        WindowSystem* ws
    );
    static void init_window_system(
        WindowSystem* ws
    );

    /* Window */

    struct Window;

    void get_window_size(
        WindowSystem* ws,
        Window* window,
        uint16_t* width,
        uint16_t* height
    );

    /* DialogWindow */

    struct DialogWindow;

    static void destroy_dialog_window(
        WindowSystem* ws,
        DialogWindow* window
    );
    static void init_dialog_window(
        WindowSystem* ws,
        DialogWindow* window,
        Window* parent,
        int32_t width,
        int32_t height
    );

    /* MainWindow */

    struct MainWindow;

    static void destroy_main_window(
        WindowSystem* ws,
        MainWindow* window
    );
    static void init_main_window(
        WindowSystem* ws,
        MainWindow* window
    );

    /* Vulkan integration */

    static VkResult create_vk_surface(
        VkSurfaceKHR* vk_surface,
        VkInstance vk_instance,
        WindowSystem* ws,
        Window* window
    );
};

} // namespace tide::graphics
